# Zigbee3-Coordinator

HomeMaster第二代网关 Zigbee协调器部分

### 该项目属于智能家居定制项目的一部分，该项目还包括:

- [**homemaster 设备接入网关, 采用Go+C开发**](https://gitee.com/adaidesigner/homemaster)
- [**homemaster-driver 网关硬件设备驱动, 由C编写的linux内核模块**](https://gitee.com/adaidesigner/homemaster-driver)
- [**zigbee3.0-coordinator 网关协调器部分, 芯片采用NXP的JN5169**](https://gitee.com/adaidesigner/zigbee3.0-coordinator)
- [**jarvis 家庭控制中枢服务端部分, 采用Go编写**](https://gitee.com/adaidesigner/jarvis)
`取名来源: JARVIS(贾维斯)钢铁侠托尼的AI助理，幻视的核心软件`
- [**home 家居ios端应用程序,兼容iphone和ipad, 由swift编写**](https://gitee.com/adaidesigner/home)

### 智能家居定制项目介绍网址: **https://adai.design/design**

## 工程说明

由于NXP提供的开发工具`Eclipse IED`界面丑陋，而且非常难用，所以利用NXP提供工具链编写了Makefile文件用于编译和下载.

- 编译工程 **make**
- 部署程序 **make flash** `(Makefile文件中串口路径:SERIAL_PORT=COM5)`
- 查询芯片信息: **make info**
- 擦除芯片: **make erase**

## CLion 开发说明

CLion采用`cmake`构建工程，详情请查看`CMakeLists.txt`

![img](Doc/edit.png)

#### 历史版本
- 2018-08-09 创建工程

----
## 智能家居定制项目介绍网址: **https://adai.design/design**



