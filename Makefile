#
# Zigbee 3.0 协调器编译
#

.PHONY: all clean info erase

LDFLAGS = TRACE=1 NODE=COORDINATOR BAUD=115200 JENNIC_CHIP=JN5169 JENNIC_CHIP_FAMILY=JN516x
BUILD_DIR = Build/ZigbeeNodeControlBridge
SERIAL_PORT=COM5

all:
	cd ${BUILD_DIR} && make $(LDFLAGS)

flash:
	JN51xxProgrammer.exe -s ${SERIAL_PORT} -f Build/ZigbeeNodeControlBridge/ZigbeeNodeControlBridge_JN5169_COORDINATOR_115200.bin -V 0

info:
	JN51xxProgrammer.exe -s ${SERIAL_PORT} --deviceconfig -V 0

erase:
	JN51xxProgrammer.exe -s ${SERIAL_PORT} --eraseeeprom=full -V 0

clean:
	cd ${BUILD_DIR} && make $(LDFLAGS) clean



